<?php

namespace Drupal\entity_expand;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Class to provide functionality for EntityExpandBase.
 */
class EntityExpandBase implements EntityExpandInterface {

  /**
   * The Entity Interface.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected EntityInterface $entity;

  /**
   * Init entity.
   *
   * Entity constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   */
  public function __construct(EntityInterface $entity) {
    $this->entity = $entity;
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Get field by name.
   *
   * @param string $field
   *   The name of the field.
   */
  public function get($field):FieldItem {
    return new FieldItem($this->entity, $field);
  }

  /**
   * Get bundle key.
   *
   * For nodes, the key is 'type'.
   * For profiles, the key is 'profile_type'.
   *
   * @return false|string
   *   The bundle key or FALSE if not found.
   */
  public function bundleKey() {
    return $this->entity->getEntityType()->getKey('bundle');
  }

  /**
   * Set field values.
   *
   * $values = [
   *   'field1' => 'value1',
   *   'field2' => 'value2',
   * ]
   *
   * @param array $values
   *   An array of field values to set.
   */
  public function setValues($values) {
    foreach ($values as $field => $value) {
      if ($this->entity->hasField($field)) {
        $this->entity->set($field, $value);
      }
    }
  }

  /**
   * Build entity view element.
   *
   * @param string $view_mode
   *   The view mode to render the entity.
   *
   * @return array
   *   The rendered entity view element.
   */
  public function view($view_mode = 'full') {
    return \Drupal::entityTypeManager()
      ->getViewBuilder($this->entity->getEntityTypeId())
      ->view($this->entity, $view_mode);
  }

  /**
   * Get entity operations.
   *
   * [
   *   'view' => [],
   *   'edit' => [],
   *   'delete' => []
   * ]
   *
   * @return mixed
   *   The entity operations array.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   If the entity type's list builder plugin is not found.
   */
  public function getOperations() {
    $entityTypeManager = \Drupal::entityTypeManager();
    $entity_type = $entityTypeManager->getDefinition($this->entity->getEntityTypeID());
    $list_builder = $entityTypeManager->createHandlerInstance(EntityListBuilder::class, $entity_type);
    return $list_builder->getOperations($this->entity);
  }

  /**
   * Call entity old functions.
   *
   * @param string $name
   *   The name of the function to call.
   * @param array $arguments
   *   The arguments to pass to the function.
   *
   * @return false|mixed
   *   The result of the function call, or FALSE if the function doesn't exist.
   */
  public function __call($name, $arguments) {
    if (method_exists($this->entity, $name)) {
      return call_user_func_array([
        $this->entity,
        $name,
      ], $arguments);
    }
    return FALSE;
  }

  /**
   * Call entity old property.
   *
   * @param string $name
   *   The name of the property.
   *
   * @return \Drupal\dyniva_helper\FieldItem
   *   A new instance of FieldItem with the specified property as the field.
   */
  public function __get($name) {
    return new FieldItem($this->entity, $name);
  }

}

/**
 * Class to provide functionality for Entity field item.
 *
 * @package Drupal\dyniva_helper
 */
class FieldItem {

  /**
   * The field item list.
   *
   * @var \Drupal\Core\Field\FieldItemListInterface
   */
  protected FieldItemListInterface $field;

  /**
   * FieldItem constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity containing the field.
   * @param mixed $field
   *   The field to be wrapped by the FieldItem object.
   */
  public function __construct(EntityInterface $entity, $field) {
    $this->field = $entity->get($field);
  }

  /**
   * Set call field old functions.
   *
   * @param string $name
   *   The name of the function to call.
   * @param array $arguments
   *   The arguments to pass to the function.
   *
   * @return false|mixed
   *   The result of the function call, or FALSE if the function doesn't exist.
   */
  public function __call($name, $arguments) {
    if (method_exists($this->field, $name)) {
      return call_user_func_array([
        $this->field,
        $name,
      ], $arguments);
    }
    return FALSE;
  }

  /**
   * Set call field old property.
   *
   * @param string $name
   *   The name of the property.
   *
   * @return mixed
   *   The value of the specified field property.
   */
  public function __get($name) {
    if ($name == 'entity') {
      return $this->ref();
    }
    return $this->field->{$name};
  }

  /**
   * Get the empty value of the field.
   *
   * @param string $default
   *   The default value to return if the field is empty.
   *
   * @return string
   *   The field's string value if its not empty; otherwise, the default value.
   */
  public function emptyValue($default = '') {
    if (!$this->field->isEmpty()) {
      return $this->field->getString();
    }
    return $default;
  }

  /**
   * Get list[Text] field label, drupal default privide key.
   *
   * @return string
   *   The label is returned. Otherwise, the default value is returned.
   */
  public function listTextLabel($default_value = '') {
    $allowvalues = $this->field->getSetting('allowed_values');
    return $allowvalues[$this->field->value] ?? $default_value;
  }

  /**
   * Get ref entity.
   *
   * @param null $default_value
   *   The default value.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   If the field is not empty and has an entity reference,
   *   otherwise the default value.
   */
  public function ref($default_value = NULL) {
    if (!$this->field->isEmpty() && $this->field->entity) {
      return entity_expand_load($this->field->entity);
    }
    return $default_value;
  }

  /**
   * Get the field value.
   *
   * @param string $default_value
   *   The default value in string.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|mixed|string
   *   The field value if it is not empty, otherwise the default value.
   */
  public function val($default_value = '') {
    return !$this->field->isEmpty() ? $this->field : $default_value;
  }

  /**
   * Get field values.
   *
   * @return array
   *   An array of field values.
   */
  public function vals($default_value = []) {
    if ($this->field->isEmpty()) {
      return [];
    }
    return array_column($this->field->getValue(), 'value');
  }

  /**
   * Get refs entity.
   *
   * @param callable|null $callable
   *   The callable to apply to each entity.
   *
   * @return array
   *   Return array value for refs.
   */
  public function refs(callable $callable = NULL) {
    if (!$this->field->isEmpty()) {
      $entitys = array_map(function ($entity) {
        return entity_expand_load($entity);
      }, $this->field->referencedEntities());

      if ($callable) {
        array_map($callable, $entitys);
      }
      return $entitys;
    }

    return [];
  }

  /**
   * Get field targets.
   *
   * @return array
   *   Return targets in array.
   */
  public function targets() {
    if ($this->field->isEmpty()) {
      return [];
    }
    return array_column($this->field->getValue(), 'target_id');
  }

}
