<?php

namespace Drupal\entity_expand;

/**
 * Interface for the Entity Expand service.
 */
interface EntityExpandInterface {

  /**
   * Get the entity.
   *
   * @return mixed
   *   The entity object.
   */
  public function getEntity();

}
