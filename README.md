HOW TO USE
---------------------

* define expand directory: src/EntityExpand
* create your expand file: UserExpand.php
<?php
namespace Drupal\my_module\EntityExpand;

use Drupal\entity_expand\EntityExpandBase;

/**
 * Provides expanded functionality for the User entity.
 */
class UserExpand extends EntityExpandBase {

  /**
   * Method A.
   */
  public function a() {}

  /**
   * Method B.
   */
  public function b() {}

  /**
   * Method C.
   */
  public function c() {}

  /**
   * Get the user's name with "newname" appended.
   *
   * @return string
   *   The modified user name.
   */
  public function newname() {
    return $this->entity->name->value . 'newname';
  }

}
?>

* define a hook import this class hook_entity_expand_load.
function mymodule_entity_expand_load($entity, $entity_type_id) {
  if ($entity_type_id == 'user') {
    $entity = new \Drupal\my_module\EntityExpand\UserExpand($entity);
  }
  // more....
  if ($entity_type_id == 'taxonomy_term') {
    $entity = new \Drupal\my_module\EntityExpand\TaxonomyTermExpand($entity);
  }
  return $entity;
}

* use this expand.
$user = entity_expand_load(\Drupal\user\Entity\User::load(1));
$user->name->value // original method.
$user->a();    // get expand entity method.
$user->b();
$user->c();
$user->newname();
$user->field_ref_user->ref()->a();  // get embed entity method.
$user->field_ref_user->ref()->b();
$user->field_ref_user->ref()->c();

default expand method.
// set multiple values
$user->setValues([
  'name' => 'new name',
  'field_a' => 'a value',
  'field_b' => 'b value',
]);
$user->field_tags->refs(function($entity) {
  print_r($entity->label());
});
$user->field_category->listTextLabel();  // display field value label.
render($user->view('full'));       // entity view.
$user->field_a->val('no value');  // get field value and set default value.
$user->field_ref_value->targets();  // get multiple target_ids.
